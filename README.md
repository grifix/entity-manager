# Description
When we use the CQRS pattern, we usually don't need all this stuff that classical ORM provides. 
We don't need relations, criteria, lazy loading etc. 
We only need to add aggregate and get aggregate from the repository. This library provides this
simple functionality, and it also provides upcasting entity structure using the
[Grifix Normalizer](https://packagist.org/packages/grifix/normalizer) library. It also provides
the optimistic lock to resolve the concurrency issue.

# Installation
`composer require grifix/entity-manager`

# Usage

Imagine that we have the class `Person` and we want to store it to the database:
```php
final class Person
{
    public function __construct(
        public readonly string $id,
        public string $firstName,
        public string $lastName
    ) {
    }
}
```

Create a table with the following structure:
```sql
create table persons
(
    id uuid constraint persons_pk primary key,
    version bigint not null,
    data    jsonb  not null
);
```

Create entity manager:
```php
$dbConnection =  \Doctrine\DBAL\DriverManager::getConnection(
    [
        'dbname' => 'dbname',
        'user' => 'user',
        'password' => 'password',
        'host' => 'host',
        'driver' => 'pdo_pgsql'
    ],
);
$entityManager = \Grifix\EntityManager\EntityManager::create($dbConnection);
```

Register the `Person` entity type:
```php
$entityManager->registerEntityType(
    'person',
    Person::class,
    'persons',
    [
        Schema::create()
            ->withStringProperty('id')
            ->withStringProperty('firstName')
            ->withStringProperty('lastName')
    ]
);
```

now you cane store the person to the database:
```php
$person = new Person(
            '46acc317-3d11-4aa5-b306-4d6a88f57378',
            'John',
            'Connor'
        );
$this->entityManager->add($person, '46acc317-3d11-4aa5-b306-4d6a88f57378');
$this->entityManager->flush();
```

and you can get it from the database:
```php
$person = $this->entityManager->get(Person::class, '46acc317-3d11-4aa5-b306-4d6a88f57378');
```

# [Entity data structure upcasting](https://gitlab.com/grifix/normalizer#upcasting)
# [Injecting dependencies inside entity](https://gitlab.com/grifix/normalizer#dependency-injection)
