<?php

declare(strict_types=1);

namespace Grifix\EntityManager;

use Doctrine\DBAL\Connection;
use Grifix\EntityManager\EntityRepository\EntityRepository;
use Grifix\EntityManager\EntityRepository\Exceptions\EntityDoesNotExistException;
use Grifix\EntityManager\EntityRepository\Exceptions\VersionConflictException;
use Grifix\EntityManager\EntityTypeRegistry\EntityType;
use Grifix\EntityManager\EntityTypeRegistry\EntityTypeRegistry;
use Grifix\EntityManager\EntityTypeRegistry\Exceptions\EntityTypeAlreadyRegistered;
use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

class EntityManager implements EntityManagerInterface
{
    public function __construct(
        private readonly NormalizerInterface $normalizer,
        private readonly EntityTypeRegistry $entityTypeRegistry,
        private readonly EntityRepository $entityRepository
    ) {
    }

    /**
     * @param Schema[] $schemas
     *
     * @throws EntityTypeAlreadyRegistered
     */
    public function registerEntityType(
        string $name,
        string $objectClass,
        string $table,
        array $schemas,
        ?VersionConverterInterface $versionConverter = null,
        array $dependencies = []
    ): void {
        $this->entityTypeRegistry->registerEntityType(new EntityType($name, $table, $objectClass,));
        $this->normalizer->registerDefaultObjectNormalizer(
            $name,
            $objectClass,
            $schemas,
            $versionConverter,
            $dependencies
        );
    }


    public function add(object $entity, string $id): void
    {
        $this->entityRepository->add($entity, $id);
    }

    /**
     * @throws EntityDoesNotExistException
     */
    public function get(string $class, string $id): object
    {
        return $this->entityRepository->get($class, $id);
    }

    /**
     * @throws VersionConflictException
     */
    public function flush(): void
    {
        $this->entityRepository->flush();
    }

    public function reset(): void
    {
        $this->entityRepository->reset();
    }

    public static function create(Connection $connection): self
    {
        $entityTypeRegistry = new EntityTypeRegistry();
        $normalizer = Normalizer::create();
        return new self(
            $normalizer,
            $entityTypeRegistry,
            new EntityRepository($entityTypeRegistry, $connection, $normalizer)
        );
    }
}
