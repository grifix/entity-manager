<?php

declare(strict_types=1);

namespace Grifix\EntityManager;

use Grifix\EntityManager\EntityRepository\Exceptions\EntityDoesNotExistException;
use Grifix\EntityManager\EntityRepository\Exceptions\VersionConflictException;
use Grifix\EntityManager\EntityTypeRegistry\Exceptions\EntityTypeAlreadyRegistered;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

interface EntityManagerInterface
{
    /**
     * @param Schema[] $schemas
     *
     * @throws EntityTypeAlreadyRegistered
     */
    public function registerEntityType(
        string $name,
        string $objectClass,
        string $table,
        array $schemas,
        ?VersionConverterInterface $versionConverter = null,
        array $dependencies = []
    ): void;

    public function add(object $entity, string $id): void;

    /**
     * @template T
     * @throws EntityDoesNotExistException
     * @param class-string<T> $class
     * @return T
     */
    public function get(string $class, string $id): object;

    /**
     * @throws VersionConflictException
     */
    public function flush(): void;

    public function reset(): void;
}
