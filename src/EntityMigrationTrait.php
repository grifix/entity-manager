<?php

declare(strict_types=1);

namespace Grifix\EntityManager;

use Doctrine\Migrations\AbstractMigration;

trait EntityMigrationTrait
{
    /**
     * @property AbstractMigration $this
     */
    protected function createEntityTable(string $schema, string $table): void
    {
        /** @var $this AbstractMigration */
        $this->assetIsApplicable();

        $primaryKeyId = $schema . '_' . $table . '_pk';
        $tableId = $this->createTableId($schema, $table);

        $sql = <<<SQL
create table $tableId
(
    id      uuid
        constraint $primaryKeyId
            primary key,
    version bigint not null,
    data    jsonb  not null
);
SQL;
        $this->addSql($sql);
    }

    protected function dropTable(string $schema, string $table): void
    {
        /** @var $this AbstractMigration */
        $this->assetIsApplicable();
        $this->addSql('drop table ' . $this->createTableId($schema, $table));
    }

    private function createTableId($schema, $table): string
    {
        return $schema . '.' . $table;
    }

    private function assetIsApplicable(): void
    {
        if (!($this instanceof AbstractMigration)) {
            throw new \Exception(
                sprintf(
                    '%s is applicable only for %s!',
                    EntityMigrationTrait::class,
                    AbstractMigration::class
                )
            );
        }
    }
}
