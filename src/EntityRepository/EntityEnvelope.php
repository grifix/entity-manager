<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityRepository;

use Grifix\BigInt\BigInt;
use Grifix\EntityManager\EntityTypeRegistry\EntityType;

final class EntityEnvelope
{
    public function __construct(
        public readonly string $id,
        public readonly object $entity,
        public readonly EntityType $type,
        private ?BigInt $version = null,
        private array $rawData = []
    ) {
        if(null === $version){
            $this->version = BigInt::create(0);
        }
    }

    public function isNew(): bool
    {
        return BigInt::create(0)->isEqualTo($this->version);
    }

    public function update(array $data): void
    {
        $this->version = $this->version->add(1);
        $this->rawData = $data;
    }

    public function getRawData(): array
    {
        return $this->rawData;
    }

    public function getNextVersion(): BigInt
    {
        return $this->version->add(1);
    }

    public function getVersion(): BigInt
    {
        return $this->version;
    }
}
