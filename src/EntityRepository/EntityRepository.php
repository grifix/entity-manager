<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityRepository;

use Doctrine\DBAL\Connection;
use Grifix\BigInt\BigInt;
use Grifix\EntityManager\EntityRepository\Exceptions\EntityAlreadyExistsException;
use Grifix\EntityManager\EntityRepository\Exceptions\EntityDoesNotExistException;
use Grifix\EntityManager\EntityRepository\Exceptions\VersionConflictException;
use Grifix\EntityManager\EntityTypeRegistry\EntityTypeRegistry;
use Grifix\Normalizer\NormalizerInterface;

final class EntityRepository
{
    /** @var EntityEnvelope[] */
    private array $envelopes = [];

    public function __construct(
        private readonly EntityTypeRegistry $entityTypeRegistry,
        private readonly Connection $connection,
        private readonly NormalizerInterface $normalizer
    ) {
    }

    public function add(object $entity, string $id): void
    {
        $this->assertEntityDoesNotExist($entity, $id);
        $this->envelopes[] = new EntityEnvelope(
            $id,
            $entity,
            $this->entityTypeRegistry->getByClass($entity::class)
        );
    }

    private function assertEntityDoesNotExist(object $entity, string $id): void
    {
        foreach ($this->envelopes as $envelope) {
            if ($entity::class === $envelope->entity::class && $id === $envelope->id) {
                throw new EntityAlreadyExistsException($id, $entity::class);
            }
        }
    }

    public function get(string $class, string $id): object
    {
        $result = $this->findInMemory($class, $id);
        if (null === $result) {
            $result = $this->findInDb($class, $id);
            if (null === $result) {
                throw new EntityDoesNotExistException($class, $id);
            }
            $this->envelopes[] = $result;
        }
        return $result->entity;
    }

    public function reset(): void
    {
        $this->envelopes = [];
    }

    private function findInMemory(string $class, string $id): ?EntityEnvelope
    {
        foreach ($this->envelopes as $entity) {
            if ($entity->type->class !== $class) {
                continue;
            }
            if ($entity->id !== $id) {
                continue;
            }
            return $entity;
        }
        return null;
    }

    private function findInDb(string $class, string $id): ?EntityEnvelope
    {
        $type = $this->entityTypeRegistry->getByClass($class);
        $row = $this->connection->fetchAssociative(
            sprintf('select * from %s where id=:id', $type->table),
            ['id' => $id]
        );
        if (!$row) {
            return null;
        }
        $rawData = json_decode($row['data'], true);
        return new EntityEnvelope(
            $id,
            $this->normalizer->denormalize($rawData),
            $type,
            BigInt::create($row['version']),
            $rawData
        );
    }

    /**
     * @throws VersionConflictException
     */
    public function flush(): void
    {
        foreach ($this->envelopes as $envelope) {
            if ($envelope->isNew()) {
                $this->insert($envelope);
            } else {
                $this->update($envelope);
            }
        }
    }

    private function insert(EntityEnvelope $envelope): void
    {
        $rawData = $this->normalizer->normalize($envelope->entity);
        $this->connection->insert(
            $envelope->type->table,
            [
                'id' => $envelope->id,
                'version' => 1,
                'data' => json_encode($rawData)
            ]
        );
        $envelope->update($rawData);
    }

    /**
     * @throws VersionConflictException
     */
    private function update(EntityEnvelope $envelope): void
    {
        if (!$this->isEntityDirty($envelope)) {
            return;
        }
        $rawData = $this->normalizer->normalize($envelope->entity);
        $affectedRows = $this->connection->update(
            $envelope->type->table,
            [
                'version' => $envelope->getNextVersion()->toString(),
                'data' => json_encode($rawData)
            ],
            [
                'id' => $envelope->id,
                'version' => $envelope->getVersion()->toString()
            ]
        );
        if ((int)$affectedRows === 0) {
            $this->reset();
            throw new VersionConflictException(
                $envelope->entity::class,
                $envelope->id,
                $envelope->getVersion()->toString()
            );
        }
        $envelope->update($rawData);
    }

    private function isEntityDirty(EntityEnvelope $envelope): bool
    {
        return $this->normalizer->normalize($envelope->entity) !== $envelope->getRawData();
    }
}
