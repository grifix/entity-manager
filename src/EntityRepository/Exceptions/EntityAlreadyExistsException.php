<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityRepository\Exceptions;

final class EntityAlreadyExistsException extends \Exception
{
    public function __construct(string $entityId, string $entityClass)
    {
        parent::__construct(
            sprintf('Entity [%s] with id [%s] already exists!', $entityClass, $entityId)
        );
    }
}
