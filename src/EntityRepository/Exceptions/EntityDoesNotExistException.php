<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityRepository\Exceptions;

final class EntityDoesNotExistException extends \Exception
{

    public function __construct(string $class, string $id)
    {
        parent::__construct(sprintf('Entity [%s] with id [%s] does not exist!', $class, $id));
    }
}
