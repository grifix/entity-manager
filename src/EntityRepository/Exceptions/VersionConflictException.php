<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityRepository\Exceptions;

final class VersionConflictException extends \Exception
{

    public function __construct(string $entityClass, string $id, string $version)
    {
        parent::__construct(
            sprintf(
                'There is a version conflict for the entity [%s] with id [%s] in version [%s]',
                $entityClass,
                $id,
                $version
            )
        );
    }
}
