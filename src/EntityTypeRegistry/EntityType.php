<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityTypeRegistry;

final class EntityType
{
    public function __construct(
        public readonly string $name,
        public readonly string $table,
        public readonly string $class
    ) {
    }

    public function findIntersect(self $other): ?string
    {
        if ($other->name === $this->name) {
            return 'name';
        }

        if ($other->table === $this->table) {
            return 'table';
        }

        if ($other->class === $this->class) {
            return 'class';
        }

        return null;
    }
}
