<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityTypeRegistry;

use Grifix\EntityManager\EntityTypeRegistry\Exceptions\EntityTypeAlreadyRegistered;
use Grifix\EntityManager\EntityTypeRegistry\Exceptions\EntityTypeIsNotRegisteredException;

final class EntityTypeRegistry
{
    /** @var EntityType[] */
    private array $entityTypes = [];

    /**
     * @throws EntityTypeAlreadyRegistered
     */
    public function registerEntityType(EntityType $entityType): void
    {
        $this->assertEntityTypeDoesNotIntersect($entityType);
        $this->entityTypes[] = $entityType;
    }

    public function getByClass(string $class): EntityType
    {
        foreach ($this->entityTypes as $entityType) {
            if ($entityType->class === $class) {
                return $entityType;
            }
        }
        throw EntityTypeIsNotRegisteredException::withClass($class);
    }

    /**
     * @throws EntityTypeAlreadyRegistered
     */
    private function assertEntityTypeDoesNotIntersect(EntityType $newEntityType): void
    {
        $exception = match ($this->findIntersect($newEntityType)) {
            'name' => EntityTypeAlreadyRegistered::withName($newEntityType->name),
            'class' => EntityTypeAlreadyRegistered::withClass($newEntityType->class),
            'table' => EntityTypeAlreadyRegistered::withTable($newEntityType->table),
            default => null
        };
        if (null !== $exception) {
            throw $exception;
        }
    }

    private function findIntersect(EntityType $newEntityType): ?string
    {
        foreach ($this->entityTypes as $entityType) {
            $intersect = $entityType->findIntersect($newEntityType);
            if (null !== $intersect) {
                return $intersect;
            }
        }

        return null;
    }

}
