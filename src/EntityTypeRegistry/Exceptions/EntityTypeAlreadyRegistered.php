<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityTypeRegistry\Exceptions;

final class EntityTypeAlreadyRegistered extends \Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function withName(string $name): self
    {
        return new self(sprintf('Entity type with name [%s] already registered!', $name));
    }

    public static function withClass(string $class): self
    {
        return new self(sprintf('Entity type with class [%s] already registered!', $class));
    }

    public static function withTable(string $table): self
    {
        return new self(sprintf('Entity type with table [%s] already registered!', $table));
    }
}
