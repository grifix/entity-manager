<?php

declare(strict_types=1);

namespace Grifix\EntityManager\EntityTypeRegistry\Exceptions;

final class EntityTypeIsNotRegisteredException extends \Exception
{

    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function withClass(string $class): self
    {
        return new self(sprintf('Entity type with class [%s] is not registered!', $class));
    }
}
