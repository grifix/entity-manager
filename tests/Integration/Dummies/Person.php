<?php

declare(strict_types=1);

namespace Grifix\EntityManager\Tests\Integration\Dummies;

final class Person
{
    public function __construct(
        public readonly string $id,
        public string $firstName,
        public string $lastName
    ) {
    }
}
