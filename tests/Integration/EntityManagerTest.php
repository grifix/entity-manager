<?php

declare(strict_types=1);

namespace Grifix\EntityManager\Tests\Integration;

use DateTime;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Logging\Middleware;
use Grifix\EntityManager\EntityManager;
use Grifix\EntityManager\EntityRepository\Exceptions\EntityAlreadyExistsException;
use Grifix\EntityManager\EntityRepository\Exceptions\EntityDoesNotExistException;
use Grifix\EntityManager\EntityRepository\Exceptions\VersionConflictException;
use Grifix\EntityManager\EntityTypeRegistry\Exceptions\EntityTypeAlreadyRegistered;
use Grifix\EntityManager\EntityTypeRegistry\Exceptions\EntityTypeIsNotRegisteredException;
use Grifix\EntityManager\Tests\Integration\Dummies\Person;
use Grifix\EntityManager\Tests\Integration\Spies\LoggerSpy;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

final class EntityManagerTest extends TestCase
{
    private readonly Connection $connection;
    private readonly EntityManager $entityManager;
    private readonly LoggerSpy $loggerSpy;

    protected function setUp(): void
    {
        $this->loggerSpy = new LoggerSpy();
        $this->connection = $this->createConnection();
        $this->entityManager = EntityManager::create($this->connection);
        $this->registerPersonEntityType($this->entityManager);
        $sql = <<<SQL
create table if not exists persons
(
    id      uuid
        constraint persons_pk
            primary key,
    version bigint not null,
    data    jsonb  not null
);
SQL;

        $this->connection->executeQuery($sql);
        $this->connection->executeQuery('truncate table persons');
        $this->loggerSpy->clear();
    }

    private function createConnection(): Connection
    {
        $config = new Configuration();
        $config->setMiddlewares([
            new Middleware(new Logger('logger', [$this->loggerSpy]))
        ]);
        return DriverManager::getConnection(
            [
                'dbname' => $_ENV['PHPUNIT_DB_NAME'],
                'user' => $_ENV['PHPUNIT_DB_USER'],
                'password' => $_ENV['PHPUNIT_DB_PASSWORD'],
                'host' => $_ENV['PHPUNIT_DB_HOST'],
                'driver' => 'pdo_pgsql'
            ],
            $config
        );
    }

    private function createEntityManager(): EntityManager
    {
        $result = EntityManager::create($this->createConnection());
        $this->registerPersonEntityType($result);
        return $result;
    }

    private function registerPersonEntityType(EntityManager $entityManager): void
    {
        $entityManager->registerEntityType(
            'person',
            Person::class,
            'persons',
            [
                Schema::create()
                    ->withStringProperty('id')
                    ->withStringProperty('firstName')
                    ->withStringProperty('lastName')
            ]
        );
    }

    public function testItGetsFromDb(): void
    {
        $person = new Person(
            '46acc317-3d11-4aa5-b306-4d6a88f57378',
            'John',
            'Connor'
        );
        $this->entityManager->add($person, '46acc317-3d11-4aa5-b306-4d6a88f57378');
        $this->entityManager->flush();
        $this->entityManager->reset();
        $this->loggerSpy->clear();
        self::assertEquals(
            $person,
            $this->entityManager->get(Person::class, '46acc317-3d11-4aa5-b306-4d6a88f57378')
        );
        self::assertCount(1, $this->loggerSpy->getRecords());
        self::assertEquals('select * from persons where id=?', $this->loggerSpy->shiftRecord()->context['sql']);
    }

    public function testItGetsFromMemory(): void
    {
        $person = new Person(
            '46acc317-3d11-4aa5-b306-4d6a88f57378',
            'John',
            'Connor'
        );
        $this->entityManager->add($person, '46acc317-3d11-4aa5-b306-4d6a88f57378');
        $this->loggerSpy->clear();
        self::assertEquals(
            $person,
            $this->entityManager->get(Person::class, '46acc317-3d11-4aa5-b306-4d6a88f57378')
        );
        self::assertCount(0, $this->loggerSpy->getRecords());
    }

    public function testItAdds(): void
    {
        $person = new Person(
            '46acc317-3d11-4aa5-b306-4d6a88f57378',
            'John',
            'Connor'
        );
        $this->entityManager->add($person, '46acc317-3d11-4aa5-b306-4d6a88f57378');
        $this->entityManager->flush();
        $record = $this->findRecord('46acc317-3d11-4aa5-b306-4d6a88f57378');
        self::assertEquals(
            [
                'id' => '46acc317-3d11-4aa5-b306-4d6a88f57378',
                'version' => 1,
                'data' =>
                    [
                        'id' => '46acc317-3d11-4aa5-b306-4d6a88f57378',
                        'lastName' => 'Connor',
                        'firstName' => 'John',
                        '__normalizer__' =>
                            [
                                'name' => 'person',
                                'version' => 1,
                            ],
                    ],
            ]
            ,
            $record
        );
    }

    public function testItUpdates()
    {
        $person = new Person(
            '46acc317-3d11-4aa5-b306-4d6a88f57378',
            'John',
            'Connor'
        );
        $this->entityManager->add($person, '46acc317-3d11-4aa5-b306-4d6a88f57378');
        $this->entityManager->flush();
        $person->firstName = 'Sarah';
        $this->entityManager->flush();
        $record = $this->findRecord('46acc317-3d11-4aa5-b306-4d6a88f57378');
        self::assertEquals(
            [
                'id' => '46acc317-3d11-4aa5-b306-4d6a88f57378',
                'version' => 2,
                'data' =>
                    [
                        'id' => '46acc317-3d11-4aa5-b306-4d6a88f57378',
                        'lastName' => 'Connor',
                        'firstName' => 'Sarah',
                        '__normalizer__' =>
                            [
                                'name' => 'person',
                                'version' => 1,
                            ],
                    ],
            ]
            ,
            $record
        );
    }

    public function testItLocksOptimistically(): void
    {
        $person = new Person(
            '46acc317-3d11-4aa5-b306-4d6a88f57378',
            'John',
            'Connor'
        );
        $this->entityManager->add($person, '46acc317-3d11-4aa5-b306-4d6a88f57378');
        $this->entityManager->flush();

        $entityManagerA = $this->createEntityManager();
        $entityManagerB = $this->createEntityManager();

        /** @var Person $personA */
        $personA = $entityManagerA->get(Person::class, '46acc317-3d11-4aa5-b306-4d6a88f57378');

        /** @var Person $personB */
        $personB = $entityManagerB->get(Person::class, '46acc317-3d11-4aa5-b306-4d6a88f57378');

        $personA->firstName = 'Sarah';
        $entityManagerA->flush();

        $personB->lastName = 'Norris';
        $this->expectException(VersionConflictException::class);
        $this->expectExceptionMessage(
            'There is a version conflict for the entity [Grifix\EntityManager\Tests\Integration\Dummies\Person] with id [46acc317-3d11-4aa5-b306-4d6a88f57378] in version [1'
        );
        $entityManagerB->flush();
        $record = $this->findRecord('46acc317-3d11-4aa5-b306-4d6a88f57378');
        self::assertEquals('Connor', $record['data']['lastName']);
        self::assertEquals('Sarah', $record['data']['firstName']);
    }

    /**
     * @dataProvider itDoesNotRegisterEntityTypeTwiceDataProvider
     */
    public function testItDoesNotRegisterEntityTypeTwice(
        string $name,
        string $objectClass,
        string $table,
        string $expectedException,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $this->entityManager->registerEntityType(
            $name,
            $objectClass,
            $table,
            [
                Schema::create()
            ]
        );
    }

    public function itDoesNotRegisterEntityTypeTwiceDataProvider(): array
    {
        return [
            'name' => [
                'person',
                \DateTime::class,
                'test',
                EntityTypeAlreadyRegistered::class,
                'Entity type with name [person] already registered!',
            ],
            'entityClass' => [
                'test',
                Person::class,
                'test',
                EntityTypeAlreadyRegistered::class,
                'Entity type with class [Grifix\EntityManager\Tests\Integration\Dummies\Person] already registered!',
            ],
            'table' => [
                'test',
                \DateTime::class,
                'persons',
                EntityTypeAlreadyRegistered::class,
                'Entity type with table [persons] already registered!',
            ]
        ];
    }

    public function testItDoesNotAddNotRegisteredEntity(): void
    {
        $this->expectException(EntityTypeIsNotRegisteredException::class);
        $this->expectExceptionMessage('Entity type with class [DateTime] is not registered!');
        $this->entityManager->add(new \DateTime(), '1');
    }

    public function testItDoesNotGetNotRegisterEntity(): void
    {
        $this->expectException(EntityTypeIsNotRegisteredException::class);
        $this->expectExceptionMessage('Entity type with class [DateTime] is not registered!');
        $this->entityManager->get(DateTime::class, '1');
    }

    public function testItDoesNotGetNotExistedEntity(): void
    {
        $this->entityManager->add(
            new Person('a469a9f6-09f1-4489-8d15-6869ae71c40b', 'John', 'Connor'),
            'a469a9f6-09f1-4489-8d15-6869ae71c40b'
        );
        $this->expectException(EntityDoesNotExistException::class);
        $this->expectExceptionMessage(
            'Entity [Grifix\EntityManager\Tests\Integration\Dummies\Person] with id [b409e790-2993-4ce5-a2b4-97608ce2f0ca] does not exist!'
        );
        $this->entityManager->get(Person::class, 'b409e790-2993-4ce5-a2b4-97608ce2f0ca');
    }

    public function testItDoesNotUpdateNotDirtyEntity(): void
    {
        $person = new Person('a469a9f6-09f1-4489-8d15-6869ae71c40b', 'John', 'Connor');
        $this->entityManager->add($person, 'a469a9f6-09f1-4489-8d15-6869ae71c40b');
        $this->entityManager->flush();
        $this->entityManager->flush();
        self::assertCount(1, $this->loggerSpy->getRecords());
    }

    public function testItDoesNotAddEntityTwice(): void
    {
        $person = new Person('a469a9f6-09f1-4489-8d15-6869ae71c40b', 'John', 'Connor');
        $this->entityManager->add($person, 'a469a9f6-09f1-4489-8d15-6869ae71c40b');
        $this->expectException(EntityAlreadyExistsException::class);
        $this->expectExceptionMessage(
            'Entity [Grifix\EntityManager\Tests\Integration\Dummies\Person] with id [a469a9f6-09f1-4489-8d15-6869ae71c40b] already exists!'
        );
        $this->entityManager->add($person, 'a469a9f6-09f1-4489-8d15-6869ae71c40b');
    }

    public function testItResets():void{
        $person = new Person('a469a9f6-09f1-4489-8d15-6869ae71c40b', 'John', 'Connor');
        $this->entityManager->add($person, 'a469a9f6-09f1-4489-8d15-6869ae71c40b');
        $this->entityManager->flush();
        $this->loggerSpy->clear();
        $this->entityManager->get(Person::class, 'a469a9f6-09f1-4489-8d15-6869ae71c40b');
        self::assertCount(0, $this->loggerSpy->getRecords());
        $this->entityManager->reset();
        $this->entityManager->get(Person::class, 'a469a9f6-09f1-4489-8d15-6869ae71c40b');
        self::assertCount(1, $this->loggerSpy->getRecords());
        $record = $this->loggerSpy->shiftRecord();
        self::assertEquals('select * from persons where id=?',$record->context['sql']);
    }

    private function findRecord(string $id): ?array
    {
        $result = $this->connection->fetchAssociative(
            'select * from persons where id=:id',
            ['id' => $id]
        );
        if (!$result) {
            return null;
        }
        $result['data'] = json_decode($result['data'], true);
        return $result;
    }

}
