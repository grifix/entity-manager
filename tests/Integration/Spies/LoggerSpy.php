<?php

declare(strict_types=1);

namespace Grifix\EntityManager\Tests\Integration\Spies;

use Monolog\Handler\HandlerInterface;
use Monolog\LogRecord;

final class LoggerSpy implements HandlerInterface
{
    private array $records = [];

    public function handleBatch(array $records): void
    {
        foreach ($records as $record) {
            $this->handle($record);
        }
    }

    public function handle(LogRecord $record): bool
    {
        $this->records[] = $record;
        return true;
    }

    public function close(): void
    {

    }

    public function isHandling(LogRecord $record): bool
    {
        return true;
    }

    public function getRecords(): array
    {
        return $this->records;
    }

    public function clear(): void
    {
        $this->records = [];
    }

    public function shiftRecord(): LogRecord
    {
        return array_shift($this->records);
    }
}
